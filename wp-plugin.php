<?php

/*
Plugin Name: WP Plugin
Description: WP Plugin Template
Version: 0.0.1
License: GPL
Text Domain: wp-p
*/


/**
 * @package modular-parent
 * Class Functions.
 */

define('WP_P_PLUGIN_FILE', __FILE__);
define('WP_P_PLUGIN_DIR', __DIR__);

if (!class_exists('\Psr4AutoloaderClass')) {
    require_once WP_P_PLUGIN_DIR . '/autoload.php';
}

$loader = new \Psr4AutoloaderClass;

$loader->addNamespace('WPP\Admin', WP_P_PLUGIN_DIR . '/Includes/Admin');

$loader->register();

new \MPP\Admin\Init();