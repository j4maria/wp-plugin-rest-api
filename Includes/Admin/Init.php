<?php

namespace MPP\Admin;

class Init
{
    public function __construct()
    {
        $this->define_hooks();
        $this->define_shortcodes();
    }

    private function define_hooks()
    {
    }

    private function define_shortcodes()
    {
    }
}